function initButtonClick() {
    $(".add-cart-btn").mouseover(function () {
        $(".add-cart-btn img").attr("src", "images/add_to_cart_btn_active.png");
    });
    $(".add-cart-btn").mouseout(function () {
        $(".add-cart-btn img").attr("src", "images/add_to_cart_btn.png");
    });

    $("#select-model button").click(function () {
        $("#select-model button").removeClass("active");
        $(this).addClass("active");
    });

    $("#select-case-color button").click(function () {
        $("#select-case-color button").removeClass("active");
        $(this).addClass("active");
    });

    $("#select-dial button").click(function () {
        $("#select-dial button").removeClass("active");
        $(this).addClass("active");
    });

    $("#select-strap button").click(function () {
        $("#select-strap button").removeClass("active");        
        $(this).addClass("active");
        var style = $(this).data('value');
        if(style=="silicon" || style=="steel" || style=="bumber" || style=="nylon"){
            $("#strap-color").hide();
            $("#band-available-color").removeClass("active");
        }
        else{
            $("#strap-color").show();
            $("#band-available-color").addClass("active");
            if(style=="mesh"){
                $("#strap-color button[data-value='gold']").show();
                $("#strap-color button[data-value='silver']").show();
                $("#strap-color button[data-value='blue']").hide();
                $("#strap-color button[data-value='brown']").hide();
                $("#strap-color button[data-value='brown']").addClass('active');
                $("#strap-color button").removeClass('active');
                $("#strap-color button[data-value='silver']").addClass('active');
            }
            else if(style=="croco"){
                $("#strap-color button[data-value='gold']").hide();
                $("#strap-color button[data-value='silver']").hide();
                $("#strap-color button[data-value='blue']").show();
                $("#strap-color button[data-value='brown']").show();
                $("#strap-color button").removeClass('active');
                $("#strap-color button[data-value='brown']").addClass('active');
            }
        }
    });

    $("#select-buckle button").click(function () {
        $("#select-buckle button").removeClass("active");
        $(this).addClass("active");
    });

    $("#band-available-color button").click(function () {
        $("#band-available-color button").removeClass("active");
        $(this).addClass("active");
    });
    $(".button-list button").click(function () {
        buildWatch();
    });
}
function changePhoto(watch){
    var filename;
    if(watch.strap_color!=""){  
        filename = watch.strap_color+"_"+watch.strap_style+".png";
    }
    else{
        filename = watch.strap_style+".png";
    }
    
    var front_url ="images/watch/"+watch.model+"/"+watch.case+"_case/"+watch.dial+"_dial/"+filename;


    if(watch.strap_color!=""){
        watch.strap_color="-"+watch.strap_color;
    }
    var left_url = "images/watch/"+watch.model+"/"+watch.case+"_case/left_side/"+watch.strap_style+watch.strap_color+"-band.png";
    var rear_url = "images/watch/"+watch.model+"/"+watch.case+"_case/rear_side/"+watch.strap_style+watch.strap_color+"-band.png";
    var right_url = "images/watch/"+watch.model+"/"+watch.case+"_case/right_side/"+watch.strap_style+watch.strap_color+"-band.png";
    
    $("#front-side").attr('src',front_url);
    $("#left-side").attr('src',left_url);
    $("#back-side").attr('src',rear_url);
    $("#right-side").attr("src",right_url)
}
function buildWatch(){
    var model = $("#select-model button.active").data('value');
    var watch_case = $("#select-case-color button.active").data('value');
    var dial = $("#select-dial button.active").data('value');
    var strap = $("#select-strap button.active").data('value');
    var buckle = $("#select-buckle button.active").data('value');
    var strap_color_;
    if($("#band-available-color").hasClass("active")){
        strap_color = $("#band-available-color button.active").data('value');
    }
    else{
        strap_color = "";
    }
    

    var watch = {
        'model':model,
        'case':watch_case,
        'dial':dial,
        'strap_style':strap,
        'strap_color':strap_color,        
        'buckle_color':buckle,
    };

    changePhoto(watch);

    var whole_watch = watch.model+"_"+watch.case+"_"+watch.dial+"_"+watch.strap_style+"_"+watch.strap_color+"_"+watch.buckle_color;
    $("#watch-id").val(whole_watch);
}
jQuery(function ($) {
    initButtonClick();    
    buildWatch();
});